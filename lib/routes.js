// ROUTES
Router.configure({
  layoutTemplate: 'layoutDefault'
});
Router.route('/', function() {
  this.render('homepage', {to: 'contentMainView'});
});
Router.route('/about/', function() {
  this.render('about', {to: 'contentMainView'});
  // eventually store the subcontentmainviews as a session variable
  // instead of using the render below.
  this.render('aboutMe', {to: 'subcontentMainView'});
});
  Router.route('/about/me/', function() {
    this.render('about', {to: 'contentMainView'});
    this.render('aboutMe', {to: 'subcontentMainView'});
  });
  Router.route('/about/my_designs/', function() {
    this.render('about', {to: 'contentMainView'});
    this.render('aboutMyDesigns', {to: 'subcontentMainView'});
  });
  Router.route('/about/this_website/', function() {
    this.render('about', {to: 'contentMainView'});
    this.render('aboutThisWebsite', {to: 'subcontentMainView'});
  });
  Router.route('/about/testimonials/', function() {
    this.render('about', {to: 'contentMainView'});
    this.render('testimonials', {to: 'subcontentMainView'});
  });
Router.route('/design/', function() {
  this.render('design', {to: 'contentMainView'});
  // eventually store the subcontentmainviews as a session variable
  // instead of using the render below.
  this.render('portfolio', {to: 'subcontentMainView'});
});
  Router.route('/design/portfolio/', function() {
    this.render('design', {to: 'contentMainView'});
    this.render('portfolio', {to: 'subcontentMainView'});
  });
  Router.route('/design/moodboards/', function() {
    this.render('design', {to: 'contentMainView'});
    this.render('moodboards', {to: 'subcontentMainView'});
    $.getScript("//assets.pinterest.com/js/pinit_main.js");
  });
Router.route('/contact/', function() {
  this.render('contact', {to: 'contentMainView'});
});
Router.route('/readme/', function() {
  this.render('recommended', {to: 'contentMainView'});
});
